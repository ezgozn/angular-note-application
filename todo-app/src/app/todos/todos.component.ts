import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Todo } from '../core/models/todo.model';
import { DataService } from '../core/services/data.service';
import { NotificationService } from '../core/services/notification.service';
import { TodoDialogComponent } from '../todo-dialog/todo-dialog.component';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  todos: Todo[]
  showValidationErrors: boolean

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.dataService.getAllTodos('toDoNotes').subscribe((result) => {
      this.todos = result;
      this.todos.sort((a, b) => new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime());
    });

  }

  onFormSubmit(form: NgForm) {
    if (form.invalid) {
      return this.showValidationErrors = true;
    }

    const dtoOut = new Todo();
    dtoOut.important = false;
    dtoOut.createdOn = new Date();
    dtoOut.note = form.value.text;
    dtoOut.title = form.value.text;
    this.dataService.addTodo(dtoOut);
    this.notificationService.showSuccess('Notunuz başarıyla eklendi.', `Not: ${dtoOut.title}`);

    this.showValidationErrors = false;
    form.reset();
  }

  tagImportant(todo: Todo) {
    todo.important = !todo.important;
    const index = this.todos.indexOf(todo);
    this.dataService.updateTodo(index, todo);
    const message = todo.important ? 'Notunuz önemli olarak etiketlendi.' : 'Notunuzun etiketi kaldırıldı.';
    this.notificationService.showSuccess(message, `Not: ${todo.title}`);
  }

  editTodo(todo: Todo) {
    const index = this.todos.indexOf(todo);

    let dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '700px',
      data: todo
    });

    dialogRef.afterClosed().subscribe((result: Todo) => {
      if (result) {
        this.dataService.updateTodo(index, result);
        this.notificationService.showSuccess('Notunuz başarıyla güncellendi.', `Not: ${result.title}`);
      }
    });
  }

  addTodo() {
    let dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '700px',
      data: new Todo()
    });

    dialogRef.afterClosed().subscribe((result: Todo) => {
      if (result) {
        this.dataService.addTodo(result);
        this.notificationService.showSuccess('Notunuz başarıyla eklendi.', `Not: ${result.title}`);
      }
    })
  }

  deleteTodo(todo: Todo) {
    const index = this.todos.indexOf(todo)
    this.dataService.deleteTodo(index)
  }

}