import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { TodosComponent } from './todos.component';
import { RouterModule, Routes } from '@angular/router';
import { TodoItemComponent } from '../todo-item/todo-item.component';
import { TodoDialogComponent } from '../todo-dialog/todo-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: TodosComponent
  }
];


@NgModule({
  declarations: [
    TodosComponent,
    TodoItemComponent,
    TodoDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class TodosModule { }
