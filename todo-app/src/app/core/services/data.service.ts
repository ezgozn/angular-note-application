import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Todo } from '../models/todo.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    private _todos = new BehaviorSubject<Array<Todo>>([]);
    public todos = this._todos.asObservable();

    constructor(private localStorageService: LocalStorageService) { }

    getAllTodos(key: string): Observable<Array<Todo>> {
        const allTodos = this.localStorageService.getFromLocal(key);
        this._todos.next(allTodos);
        return this.todos;
    }

    addTodo(todo: Todo) {
        const todos = this._todos.value || [];
        todos.push(todo);
        this.localStorageService.setOnLocal('toDoNotes', todos);
        this._todos.next(todos);
    }

    updateTodo(index: number, updatedTodo: Todo) {
        const todos = this._todos.value;
        todos[index] = updatedTodo;
        this.localStorageService.setOnLocal('toDoNotes', todos);
        this._todos.next(todos);
    }

    deleteTodo(index: number) {
        const todos = this._todos.value;
        todos.splice(index, 1)
        this.localStorageService.setOnLocal('toDoNotes', todos);
        this._todos.next(todos);
    }

}