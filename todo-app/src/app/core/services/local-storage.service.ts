import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  constructor(@Inject(PLATFORM_ID) private platformId) { }

  getFromLocal(key: string): Array<Todo> {
    if (this.isLocalStorageSupported) {
      return JSON.parse(localStorage.getItem(key));
    }
    return null;
  }

  setOnLocal(key: string, value: Array<Todo>): boolean {
    if (this.isLocalStorageSupported) {
      localStorage.setItem(key, JSON.stringify(value));
      return true;
    }
    return false;
  }

  removeFromLocal(key: string): boolean {
    if (this.isLocalStorageSupported) {
      localStorage.removeItem(key);
      return true;
    }
    return false;
  }

  get isLocalStorageSupported(): boolean {
    return isPlatformBrowser(this.platformId);
  }
}
