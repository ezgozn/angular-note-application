export class Todo {
    title: string;
    note: string;
    important: boolean;
    createdOn: Date;
}