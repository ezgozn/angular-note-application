import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Todo } from '../core/models/todo.model';

@Component({
  selector: 'app-todo-dialog',
  templateUrl: './todo-dialog.component.html',
  styleUrls: ['./todo-dialog.component.scss']
})
export class TodoDialogComponent implements OnInit {
  isEdit = false;
  formSubmitted = false;

  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public todo: Todo) { }

  ngOnInit(): void {
    if (this.todo.createdOn) {
      this.isEdit = true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  onFormSubmit(form: NgForm) {
    if (form.invalid) return this.formSubmitted = true;

    if (!this.isEdit) {
      this.todo.createdOn = new Date();
      this.todo.important = false;
    }
    console.log(this.todo);

    this.dialogRef.close(this.todo);
  }

}
